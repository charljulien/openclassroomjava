package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement;

public class Pacifiste implements EspritCombatif {
    @Override
    public void combat() {
        System.out.println("Je ne combats pas !");
    }
}
