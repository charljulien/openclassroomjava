package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Perso;

import OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement.*;

public class Civil extends Personnage{
    public Civil() {}

    public Civil(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }
}
