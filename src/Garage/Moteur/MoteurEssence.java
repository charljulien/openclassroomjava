package Garage.Moteur;

import java.io.Serializable;

public class MoteurEssence extends Moteur implements Serializable {
    public MoteurEssence(String cyclindre, Double prix) {
        super(cyclindre, prix);
//        this.setCyclindre(cyclindre);
//        this.setPrix(prix);
        setType(TypeMoteur.ESSENCE);
    }
}
