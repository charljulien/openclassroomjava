package Garage.Moteur;

import java.io.Serializable;

public class MoteurElectrique extends Moteur implements Serializable {
    public MoteurElectrique(String cyclindre, Double prix) {
        super(cyclindre, prix);
//        this.setCyclindre(cyclindre);
//        this.setPrix(prix);
        setType(TypeMoteur.ELECTRIQUE);
    }
}
