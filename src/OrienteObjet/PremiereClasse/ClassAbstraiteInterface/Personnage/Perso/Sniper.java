package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Perso;

import OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement.*;

public class Sniper extends Personnage{

    public Sniper(){
        this.espritCombatif = new CombatPistolet();
    }
    public Sniper(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }
}
