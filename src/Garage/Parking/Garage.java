package Garage.Parking;

import Garage.Véhicule.Vehicule;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Garage {
    List<Vehicule> voitures = new ArrayList<>();

    public void addVoiture(Vehicule véhicule){
        if (véhicule != null){
            voitures.add(véhicule);
            ObjectInputStream ois;
            ObjectOutputStream oos;
            try {
                oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("garage.txt"))));
                oos.writeObject(véhicule);
                oos.close();
                ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("garage.txt"))));
                try {
                    System.out.println(((Vehicule)ois.readObject()).toString());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                ois.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "Voitures dans le garage\n" + voitures;
    }
}
