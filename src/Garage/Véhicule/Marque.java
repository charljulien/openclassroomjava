package Garage.Véhicule;

public enum  Marque {
    RENAULT("Renault"),
    PIGEOT("Pigeot"),
    TROEN("Troen");

    private String marque = "";

    Marque(String marque){
        this.marque = marque;
    }
}
