package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement;

public class Marcher implements Deplacement {
    @Override
    public void deplace() {
        System.out.println("Je me déplace en marchant.");
    }
}
