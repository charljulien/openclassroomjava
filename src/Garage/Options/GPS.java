package Garage.Options;

import java.io.Serializable;

public class GPS implements Option, Serializable {
    @Override
    public double getPrix() {
        return 500;
    }

    @Override
    public String toString() {
        return "GPS, prix = " + getPrix();
    }
}
