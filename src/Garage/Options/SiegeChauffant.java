package Garage.Options;

import java.io.Serializable;

public class SiegeChauffant implements Option, Serializable {

    @Override
    public double getPrix() {
        return 2000;
    }

    @Override
    public String toString() {
        return "SiegeChauffant, prix = " + getPrix();
    }
}
