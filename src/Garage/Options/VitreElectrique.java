package Garage.Options;

import java.io.Serializable;

public class VitreElectrique implements Option, Serializable {
    @Override
    public double getPrix() {
        return 1000;
    }

    @Override
    public String toString() {
        return "VitreElectrique, prix = " + getPrix();
    }
}
