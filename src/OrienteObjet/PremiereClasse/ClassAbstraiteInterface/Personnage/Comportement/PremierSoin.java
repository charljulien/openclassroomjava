package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement;

public class PremierSoin implements Soin {
    @Override
    public void soigner() {
        System.out.println("Je donne les premiers soins.");
    }
}
