package OrienteObjet.PremiereClasse;

class NombreHabitantException extends Exception{
    public NombreHabitantException(int pNbre){
        System.out.println("Vous essayez d'instancier une classe Ville avec un nombre d'habitants négatif !");
        System.out.println("\t => " + pNbre);
    }
}
