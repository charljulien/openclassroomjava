package Garage.Véhicule;

import Garage.Moteur.Moteur;
import Garage.Options.Option;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Vehicule implements Serializable {
    private double prix = 0;
    private double prixTotalOption = 0;
    private String nom = "";
    List<Option> options = new ArrayList<>();
    private Marque nomMarque;
    private Moteur moteur;

    public void setMoteur(Moteur moteur) {
        this.moteur = moteur;
    }

    public void addOption(Option option){
        if (option != null){
            prixTotalOption += option.getPrix();
            options.add(option);
        }
    }

    public double getPrix() {
        return prix;
    }

    public List<Option> getOptions() {
        return options;
    }

    public Marque getNomMarque() {
        return nomMarque;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setNomMarque(Marque nomMarque) {
        this.nomMarque = nomMarque;
    }

    @Override
    public String toString() {
        return "\n"+nomMarque +" "+ nom + "\n" + "Prix du véhicule seul = " + prix + " euros" + "\n"+ moteur +"\n"+ options+"\n"+ "Prix total des options = " + prixTotalOption+ " euros" + "\n"+ "Prix total du véhicule = " + (prix +
                prixTotalOption + moteur.getPrix())+ " euros" + "\n";
    }
}
