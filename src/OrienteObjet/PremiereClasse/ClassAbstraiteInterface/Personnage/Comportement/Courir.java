package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement;

public class Courir implements Deplacement {
    @Override
    public void deplace() {
        System.out.println("Je me déplace en courant.");
    }
}
