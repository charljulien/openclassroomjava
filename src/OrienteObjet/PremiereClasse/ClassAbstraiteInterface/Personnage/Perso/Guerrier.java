package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Perso;

import OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement.*;

public class Guerrier extends Personnage {

    public Guerrier(){
        this.espritCombatif = new CombatPistolet();
    }
    public Guerrier(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }
}
