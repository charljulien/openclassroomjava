package Garage.Options;

import java.io.Serializable;

public class BarreDeToit implements Option, Serializable {
    @Override
    public double getPrix() {
        return 500;
    }

    @Override
    public String toString() {
        return "BarreDeToit, prix = " + getPrix();
    }
}
