package Garage.Moteur;

import java.io.Serializable;

public class MoteurHybride extends Moteur implements Serializable {
    public MoteurHybride(String cyclindre, Double prix) {
        super(cyclindre, prix);
//        this.setCyclindre(cyclindre);
//        this.setPrix(prix);
        setType(TypeMoteur.HYBRIDE);
    }
}
