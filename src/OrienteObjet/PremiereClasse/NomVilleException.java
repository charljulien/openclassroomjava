package OrienteObjet.PremiereClasse;

public class NomVilleException extends Exception {
    public NomVilleException(String message){
        super(message);
    }
}
