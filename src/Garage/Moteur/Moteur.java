package Garage.Moteur;

public class Moteur {
    private TypeMoteur type;
    private String cyclindre;
    private Double prix = 0.0;

    public Moteur() {
//        this.type = null;
//        this.cyclindre = "N/A";
//        this.prix = 0.00;
    }
    public Moteur(String cyclindre) {
        this.cyclindre = "N/A";
    }

    public Moteur(String cyclindre, Double prixmoteur) {
        this.cyclindre = cyclindre;
        this.prix = prixmoteur;
    }

    public void setType(TypeMoteur type) {
        this.type = type;
    }

    public TypeMoteur getType() {
        return type;
    }

    public String getCyclindre() {
        return cyclindre;
    }

    public Double getPrix() {
        return prix;
    }

    public void setCyclindre(String cyclindre) {
        this.cyclindre = cyclindre;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }







    @Override
    public String toString() {
        return "Motorisation "+ getCyclindre() +", prix = " + getPrix() + " euros.";
    }
}
