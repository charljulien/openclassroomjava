package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Perso;
import OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement.*;

public class Medecin extends Personnage{
    public Medecin() {
        this.soin = new PremierSoin();
    }
    public Medecin(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }
}
