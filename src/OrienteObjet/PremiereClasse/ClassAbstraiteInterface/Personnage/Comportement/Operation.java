package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement;

public class Operation implements Soin{
    @Override
    public void soigner() {
        System.out.println("Je pratique des opérations !");
    }
}
