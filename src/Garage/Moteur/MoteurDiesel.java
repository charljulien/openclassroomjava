package Garage.Moteur;

import java.io.Serializable;

public class MoteurDiesel extends Moteur implements Serializable {
    public MoteurDiesel( String cyclindre, Double prix) {
        super(cyclindre, prix);
//        this.setCyclindre(cyclindre);
//        this.setPrix(prix);
        this.setType(TypeMoteur.DIESEL);
    }
}
