package InteragirAvecDesboutons;


import java.awt.BorderLayout;
import java.awt.*;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;


public class Fenetre extends JFrame{

    private Panneau pan = new Panneau();
    private Bouton bouton = new Bouton("bouton 1");
    private Bouton bouton2 = new Bouton("bouton 2");
    private JPanel container = new JPanel();
    private JLabel label = new JLabel("Le JLabel");
    private int compteur = 0;
    private boolean animated = true;
    private boolean backX, backY;
    private int x, y;

    public Fenetre() {
        this.setTitle("Animation");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        container.setBackground(Color.white);
        container.setLayout(new BorderLayout());
        container.add(pan, BorderLayout.CENTER);

        bouton.addActionListener(new BoutonListener());
        bouton2.addActionListener(new Bouton2Listener());
        bouton.setEnabled(false);

        JPanel south = new JPanel();
        south.add(bouton);
        south.add(bouton2);
        container.add(south, BorderLayout.SOUTH);
        Font police = new Font("Tahoma", Font.BOLD, 16);
        label.setFont(police);
        label.setForeground(Color.blue);
        label.setHorizontalAlignment(JLabel.CENTER);
        container.add(label, BorderLayout.NORTH);
        this.setContentPane(container);
        //this.setLayout(new BorderLayout());
            /*GridLayout gl = new GridLayout();
            gl.setColumns(2);
            gl.setRows(3);
            gl.setHgap(5); //Cinq pixels d'espace entre les colonnes (H comme Horizontal)
            gl.setVgap(5); //Cinq pixels d'espace entre les lignes (V comme Vertical)
            this.setLayout(gl);
            //On ajoute le bouton au content pane de la JFrame
            this.getContentPane().add(new JButton("1"));
            this.getContentPane().add(new JButton("2"));
            this.getContentPane().add(new JButton("3"));
            this.getContentPane().add(new JButton("4"));
            this.getContentPane().add(new JButton("5"));*/
        this.setVisible(true);
        go();
    }


/*

            bouton.addActionListener(new BoutonListener());
            bouton.setEnabled(false);
            bouton2.addActionListener(new Bouton2Listener());

            JPanel south = new JPanel();
            south.add(bouton);
            south.add(bouton2);
            container.add(south, BorderLayout.SOUTH);
            Font police = new Font("Tahoma", Font.BOLD, 16);
            label.setFont(police);
            label.setForeground(Color.blue);
            label.setHorizontalAlignment(JLabel.CENTER);
            container.add(label, BorderLayout.NORTH);
            this.setContentPane(container);
            this.setVisible(true);
            go();
        }*/

        private void go(){
            //Les coordonnées de départ de notre rond
            x = pan.getPosX();
            y = pan.getPosY();
            boolean backX = false;
            boolean backY = false;
            //Dans cet exemple, j'utilise une boucle while
            //Vous verrez qu'elle fonctionne très bien
            while (this.animated) {
                if (x < 1) backX = false;
                if (x > pan.getWidth() - 50) backX = true;
                if (y < 1) backY = false;
                if (y > pan.getHeight() - 50) backY = true;
                if (!backX) pan.setPosX(++x);
                else pan.setPosX(--x);
                if (!backY) pan.setPosY(++y);
                else pan.setPosY(--y);
                pan.repaint();

                try {
                    Thread.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        class BoutonListener implements ActionListener {
            public void actionPerformed(ActionEvent arg0) {
                animated = true;
                bouton.setEnabled(false);
                bouton2.setEnabled(true);
                go();
            }
        }

        class Bouton2Listener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                animated = false;
                bouton.setEnabled(true);
                bouton2.setEnabled(false);
            }
        }
    }

    /*public void actionPerformed(ActionEvent arg0) {
        //this.compteur++;
        label.setText("Vous avez cliqué " + this.compteur + " fois");
        if(arg0.getSource() == bouton)
            label.setText("Vous avez cliqué sur le bouton 1");

        if(arg0.getSource() == bouton2)
            label.setText("Vous avez cliqué sur le bouton 2");
    }*/




