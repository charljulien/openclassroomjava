package OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Perso;

import OrienteObjet.PremiereClasse.ClassAbstraiteInterface.Personnage.Comportement.*;

public class Chirurgien extends Personnage{
    public Chirurgien() {
        this.soin = new PremierSoin();
    }

    public Chirurgien(EspritCombatif esprit, Soin soin, Deplacement dep) {
        super(esprit, soin, dep);
    }
}
